

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Christian Burell - Information Technology Major

### Project 1 Requirements:


#### README.md file should include the following items:
1. Create a launcher icon image and display it in both activities
2. Must add background color(s) to both activities
3. Must add border around image and button
4. Must add text shadow (button)


> This is a blockquote.
> 

#### Assignment Screenshot :
First                                        |  Second 
:------------------------------------------:|:------------------------------------------:
![First Interface](img/first.png)          |![Second Interface](img/second.png)




