

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Christian Burell - Information Technology Major

### Assignment 2 Requirements:


#### README.md file should include the following items:

1. Android Recipe App Screenshots 



#### Assignment Screenshots:


*Screenshot of Recipe Home page*:

![Home Screen](img/home.png)

*Screenshot of Recipe Page*:

![Recipe Page](img/recipe.png)





