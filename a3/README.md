

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Christian Burell - Information Technology Major

### Assignment 3 Requirements:


#### README.md file should include the following items:

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records each table)
3. Screenshot of running applications first and second user interface
4. Links to the following files:
    - a3.mwb
    - a3.sql
 



#### Assignment Screenshot and Links:
Fist                                        |  Second 
:------------------------------------------:|:------------------------------------------:
![First Interface](img/first.png)          |![Second Interface](img/second.png)

MYSQL                          |  
:------------------------------------------:|
![ERD](img/erd.png)     |




[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")



