

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Christian Burell - Information Technology Major

### Assignment 1 Requirements:


1. Distrubuted version control with Git and Bitbucket
2. Development installations
3. Chapter questions

#### README.md file should include the following items:

+ Screenshot of AMPPS Installations
+ Screenshot of running Java - Hello
+ Screenshot of running app android studio
+ git commands w/short descriptions
+ Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - this command an empty Git repository. 
2. git status - displays paths that have differences between the index file and the current head commit.
3. git add . - This command updates the index using the current content found in the working tree.
4. git commit -m "description" - stores the current contents of the index in a new commit along with a log message from the user describing the changes.
5. git push -u origin master - updates remote refs using local refs, while sending objects necessary to complete the given refs.
6. git pull - Incorporates changes from a remote repository into the current branch
7. git clone - clones a repository into a newly created directory.
 


#### Assignment Screenshots:


*Screenshots*:

![ammps](img/ammps.png)

![HelloWorld](img/helloworld.png)

![firstapp](img/firstapp.png)


