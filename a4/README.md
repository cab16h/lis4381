

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Christian Burell - Information Technology Major

### Assignment 4 Requirements:


#### README.md file should include the following items:

1. Edit index.php files
2. Course title, your name, assignment requirements
3. Screenshot of running application
4. Links to the following files:
    - http://localhost/repos/lis4381/a4/index.php
    
 



#### Assignment Screenshot and Links:
First                                        |  Second 
:------------------------------------------:|:------------------------------------------:
![First](img/first.png)          |![Second](img/second.png)

HOME                         |  
:------------------------------------------:|
![home](img/home.png)     |







