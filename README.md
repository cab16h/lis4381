> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381 MOBILE WEB APPLICATION DEVELOPMENT

## Christian Burell

### Lis 4381 Requirements: 

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install android Studio and create my first app
    - Provide screenshots of installation 
    - Create bitbucket repo
    - Complete bitbucket tutorial 
    - Provide git commands

2. [A2 README.md](a2/README.md "My A1 README.md file")
    - Create Healthy Recipes Android App
    - Provide screenshots of completed app


3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD based upon buisness rules 
    - Screenshot completed ERD
    - DB resource links
    

4. [P1 README.md](p1/README.md "My p1 README.md file")
    - Screenshot of running Buisness Card first user interface
    - Screenshot of running Buisness Card second user interface
    

5. [A4 README.md](a4/README.md "My a4 README.md file")
    - Clone starter files from bitbucket
    - Review subdirectories and files
    - Review and edit index.php
    - Add jQuery validations


6. [A5 README.md](a5/README.md "My a5 README.md file")
    - Server-side validation
    - SQL forward engineering
    - Create error.php

   
7. [P2 README.md](p2/README.md "My p2 README.md file")
    - Server side validation
    - Turn off client-side validation
    - Add server-side validation and regular expressions-- as per the database entity attribute requirements
